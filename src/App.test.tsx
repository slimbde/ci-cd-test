import React from 'react';
import App from './App';
import { render } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';

it('runs the test', () => {
  expect(true).toBe(true);
});

it('renders vite logo', () => {
  render(
    <MemoryRouter>
      <App />
    </MemoryRouter>
  );

  const logo = document.getElementById('vite-logo');
  expect(logo).not.toBeNull();
});
